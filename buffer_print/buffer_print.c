#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "store_text.h"
#include "text_length.h"
#include "window_x.h"
#include "window_y.h"
#include "buffer_print.h"
#include "font.h"

struct buffer_print {
	int connected;
	m_uword address;
	m_bus *bus;
	font *ourfont;
	
};
char characters;

buffer_print *buffer_print_new(m_uword address) {
		
		
	

	buffer_print *t = malloc(sizeof(buffer_print));
	if(t) {		
		t->bus = (m_bus *)malloc(sizeof(m_bus));
		char *pPath;
                pPath = getenv("SIGNBOARDPATH");
                t->ourfont = load_font(pPath);

		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;
		
		}
	}
	return t;
}

void buffer_print_bus_connector_set(buffer_print *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void buffer_print_free(buffer_print *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void buffer_print_clock(buffer_print *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && t->bus->rW && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		int i;
			characters = t->bus->data; //gets value for characters from bus
		
			if(characters == 0){				
				for(i=0; i < text_length; i++){ //sets for loop with size of text_length
					
					print_font_char(t->ourfont, &array[i], win_x, win_y); //prints character from ourfont at coordinates
					win_x +=8;	//increments win_x by 8
				}
			}
			else if((characters > 0)&&(characters <= text_length)){ //if characters is above 0 and not more than text_length
				
				for(i=0; i < characters; i++){ //until i is more than characters
					
					print_font_char(t->ourfont, &array[i], win_x, win_y); //prints character from ourfont at coordinates
					win_x +=8; //increments win_x by 8
				}			
			
			}
			else {
				printw("Number out of range\n");
			}
		
        
       
	}
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus buffer_print_get_bus(buffer_print *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void buffer_print_set_bus(buffer_print * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
