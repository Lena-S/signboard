#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "column.h"

struct p_column {
	int connected;
	m_uword address;
	m_bus *bus;	
};
int ColumnMaker = 0;
int ColumnSet=0;
int Column;
int height=20;

p_column *p_column_new(m_uword address) {
		
		
	

	p_column  *t = malloc(sizeof(p_column ));
	if(t) {

		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
			
		}
	}
	return t;
}

void p_column_bus_connector_set(p_column  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void p_column_free(p_column *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void p_column_clock(p_column *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address)&& t->bus->rW && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		Column = t->bus->data; //gets value for column from bus
		while( ColumnMaker < height) //while control variable is less than height
		{
			mvaddch(ColumnMaker,ColumnSet,'o'); //adds a character at set coordinates
			ColumnMaker++; //increments control variable
		}
		ColumnMaker = 0; //resets control variable.
       
       	 	
        }
       
	
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus p_column_get_bus(p_column *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void p_column_set_bus(p_column * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
