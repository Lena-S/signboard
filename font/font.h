#ifndef _FONT_H_
#define _FONT_H_

/*
 * Below is an example of the data structure used to load and store
 * our letters from the binary PBM font image.  The structure holds
 * 256 letters (assumming PBM_CHAR_CNT = 256).  In this example,
 * each letter has 4 rows of 3 pixels.  letters[i] points to the
 * row data of the i-th ASCII character.  Similarly, letters[i][j]
 * points to the j-th row of pixels (as a NULL-terminated string)
 * in the i-th ASCII character.
 *
 * In the structure below, the 3 x 4 ASCII character #0 looks like
 * this:
 *
 * x x
 * xxx
 *  xx
 * xx
 *
 *           0 1 2 3 4 5 6 7 8 9        255
 *          +-+-+-+-+-+-+-+-+-+-+--   --+-+
 * letters  | | | | | | | | | | |  ...  | |
 *          +-+-+-+-+-+-+-+-+-+-+--   --+-+
 *           | | | |
 *           | | |  `----------------.
 *           | |  `----------.       |
 *           |  `----.       |       |
 *           |       |       |       |
 *           V       V       V       V
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+---
 *  (rows)  | | | | | | | | | | | | | | | | | | | | | | | |  ...
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+---
 *           | | | |
 *           | | |  `----------------------------------------.
 *           | |  `------------------------- .               |
 *           |  `------------.               |               |
 *           |               |               |               |
 *           V               V               V               V
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---
 *  (cols) | x |   | x |\0 | x | x | x |\0 |   | x | x |\0 | x | x |   |\0 |  ...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---
 *
 * The dimensions of each letter is determined by dividing automagically by
 * dividing the width and height of the PBM font image by 16.
 */



/*
 * Acceptable PBM fonts are assumed to be 16 x 16 characters big. The
 * number of rows and columns per character are up to the font designer.
 */
#define PBM_DIMENSION_SIZE 16
#define PBM_CHAR_CNT (PBM_DIMENSION_SIZE * PBM_DIMENSION_SIZE)

typedef struct font font;

struct font {
	int width;
	int height;
	/*
	 * Each letter is "height" rows of "width" characters. To keep rendering simple,
	 * each row is a NULL terminated string to allow for the row's string to be
	 * printed rather than looping through each character in the row explicitly.
	 */
    char **letters[PBM_CHAR_CNT];
};

extern font *load_font(char *filename);
extern void free_font(font *f);
extern void print_font_string(font *f, char *str, int x, int y); /* make sure it's NULL terminiated!!! */
extern void print_font_char(font *f, char *ch, int x, int y);
extern void print_font_sheet(font *f);

#endif /* _FONT_H_ */
