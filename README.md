# MiniAT Signboard
![MiniAT Signboard](http://i.imgur.com/uxFC4ei.png)

Welcome to the MiniAT Signboard.  This product uses the MiniAT technology to simulate a fully customizable LED signboard.  With our signboard, there are no limits as to what you can do outside of the programmer’s ability.

The signboard and MiniAT are controlled via assembly.  Information regarding the components of the signboard can be found below, along with installation instructions.
##Installating the MiniAT Signboard
Prior to attempting to install the MiniAT Signboard, please ensure that you have the following:

- A working MiniAT installation with appropraite environment variables set for the runtime and library path.
- NCurses installed to your system.  If using Ubuntu, this can be installed by running the command `sudo apt-get install libncurses5-dev`
- Access to the bash shell (this installation guide expects use of the bash shell).

To install the signboard for use with the MiniAT:

- Clone the contents this repository into your MiniAT system folder with the following command: `git clone https://bitbucket.org/miniat_amos/signboard.git`
- Navigate to the root MiniAT directory, and run `scons`.  This will build the MiniAT Signboard for use with the MiniAT.

##Setting the Font Path
Our signboard allows you to choose which font you would like to use.  You can use our pre-defined font, or you can create your own and save it as a .pbm file.  In order to tell the signboard which font you would like to use, you must set an environment variable containing the path to the font.

To set your path to the font in Linux, please do the following:

- If you plan on using the default font and have placed the Miniat in the ~/miniat/ folder, enter into the terminal `export SIGNBOARDPATH=~/miniat/system/signboard/font/`.  Please be sure to include the / at the end of the path.
- If you installed the miniat elsewhere, or have your own font, use the same formatting to set the font, with the path leading to the folder containing the font.
- If you plan to always use the same font, it may be handy to add this line to your startup scripts (e.g., `.bashrc` or `.profile`) so you don't have to repeat this each terminal session. 

Failure to complete these steps will result in the signboard giving an error upon launch.

##Running the MiniAT Signboard

Now that you've installed and built the MiniAT Signboard, and have correctly set your Font Path, its time to run the Signboard.

If you are using the default font provided with this system, you may use the following to run the signboard:

`signboard [[path to bin file]]`

If you have your own font, you may specify the name of the font via command line arguments as follows:

`signboard [[path to bin file]] -f [[font filename]]`

###Pre-made Demos

If you're just looking to see the MiniAT in action, a number of demos have been provided which may be run.  These may be run from the root MiniAT directory as follows:

- "Letter by Letter" Scrolling: `signboard ./out/bin/signboard.bin`
- Standard Text Scrolling: `signboard ./out/bin/signboard1.bin`
- "Letters in a Curve": `signboard ./out/bin/signboard2.bin`

##Integrating your own Assembly Programs into the MiniAT Signboard

If you've created your own assembly programs and would like to run them with the MiniAT Signboard, simply do the following:

- Add the .asm file to the signboard folder, contained in the MiniAT System directory.
- Edit the SConscript file in the signboard directory, adding the following line: `buildMash('[[.asm file name]]')`
- Navigate to the root MiniAT directory and run `scons` to integrate your new program into the MiniAT signboard build.

If you're looking to make an assembly program to use with the MiniAT Signboard but aren't quite sure where to start, you may be interested in the following resources:

- MiniAT Assembly documentation: This is present in the MiniAT repository, via the path `vm/doc/instruction_set.pdf`.
- Available features & peripherals: These are present in the `features.md` file contained within this repository.
