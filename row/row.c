#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "row.h"

struct p_row {
	int connected;
	m_uword address;
	m_bus *bus;	
};
int RowMaker = 0;
int RowSet = 0;
int width=10;

p_row *p_row_new(m_uword address) {
		
		
	

	p_row  *t = malloc(sizeof(p_row ));
	if(t) {

		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
			
		}
	}
	return t;
}

void p_row_bus_connector_set(p_row  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void p_row_free(p_row *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void p_row_clock(p_row *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address)&& t->bus->rW && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		RowSet = t->bus->data; //gets value for row from bus
		while( RowMaker < width) //while control variable is less than width
		{
			mvaddch(RowSet,RowMaker,'o'); //adds a character at set coordinates
			RowMaker++; //increments control variable
		}
		RowMaker = 0; //resets control variable.
       
       	 	
        }
       
	
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus p_row_get_bus(p_row *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void p_row_set_bus(p_row * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
