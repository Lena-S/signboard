#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "window_x.h"

struct window_x {
	int connected;
	m_uword address;
	m_bus *bus;	
};
//
int win_x=0;

window_x *window_x_new(m_uword address) {
		
		
	

	window_x  *t = malloc(sizeof(window_x ));
	if(t) {

		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
			
		}
	}
	return t;
}

void window_x_bus_connector_set(window_x  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void window_x_free(window_x *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void window_x_clock(window_x *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		if(t->bus->rW){	
			if((win_x >= 0) && (win_x < 100)){
			win_x = t->bus->data;
			}
			else{
				printw("Number out of range\n");
			}
        }
        else{        	
        		t->bus->data=win_x;        	
       	 	}
       	 	
        }
       
	
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus window_x_get_bus(window_x *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void window_x_set_bus(window_x * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
