#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "text_length.h"

struct p_text_length {
	int connected;
	m_uword address;
	m_bus *bus;	
};

int text_length=0;

p_text_length *p_text_length_new(m_uword address) {
		
		
	

	p_text_length  *t = malloc(sizeof(p_text_length ));
	if(t) {

		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
			
		}
	}
	return t;
}

void p_text_length_bus_connector_set(p_text_length  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void p_text_length_free(p_text_length *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void p_text_length_clock(p_text_length *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		if(t->bus->rW){	//if rW flag is set in bus
			if((text_length >= 0) && (text_length < 16)){ //if text_length is within boundaries
				text_length = t->bus->data; //gets text length from bus
			}
			else{
				printw("Number out of range\n");
			}
        }
        else{      
        			
        		t->bus->data=(char)text_length;   //sends text_length through bus     	
       	 	}
       	 	
        }
       
	
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus p_text_length_get_bus(p_text_length *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void p_text_length_set_bus(p_text_length * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
