#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "clear_screen.h"

struct clear_screen {
	int connected;
	m_uword address;
	m_bus *bus;	
};


clear_screen *clear_screen_new(m_uword address) {
		
		
	

	clear_screen  *t = malloc(sizeof(clear_screen ));
	if(t) {

		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
			
		}
	}
	return t;
}

void clear_screen_bus_connector_set(clear_screen  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void clear_screen_free(clear_screen *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void clear_screen_clock(clear_screen *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && t->bus->rW &&!t->bus->ack) {
		t->bus->ack = M_HIGH;
				
				clear();
				refresh();
			
        }
       
	
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus clear_screen_get_bus(clear_screen *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void clear_screen_set_bus(clear_screen * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
